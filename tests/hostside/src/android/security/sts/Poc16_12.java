/**
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.security.sts;

import android.platform.test.annotations.RootPermissionTest;
import com.android.tradefed.testtype.DeviceJUnit4ClassRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

@RunWith(DeviceJUnit4ClassRunner.class)
public class Poc16_12 extends SecurityTestCase {

    //Criticals
    /**
     *  b/32700935
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_8435() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/dev/dri/renderD129")) {
            AdbUtils.runPoc("CVE-2016-8435", getDevice(), 60);
        }
    }

    /**
     *  b/31568617
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_9120() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/dev/ion")) {
            AdbUtils.runPoc("CVE-2016-9120", getDevice(), 60);
        }
    }

    //Highs
    /**
     *  b/31243641
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_8444() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/dev/v4l-subdev17")) {
            AdbUtils.runPoc("CVE-2016-8444", getDevice(), 60);
        }
    }

    /**
     *  b/31791148
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_8448() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/dev/graphics/fb0")) {
            AdbUtils.runPoc("CVE-2016-8448", getDevice(), 60);
        }
    }

    /**
     *  b/31798848
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_8449() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/dev/tegra_avpchannel")) {
            AdbUtils.runPoc("CVE-2016-8449", getDevice(), 60);
        }
    }

    /**
     *  b/32402548
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2017_0403() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runPoc("CVE-2017-0403", getDevice(), 60);
    }

    /**
     *  b/32178033
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_8451() throws Exception {
        enableAdbRoot(getDevice());
        String command =
            "echo AAAAAAAAA > /sys/devices/f9924000.i2c/i2c-2/2-0070/power_control";
        AdbUtils.runCommandLine(command, getDevice());
    }

    /**
     *  b/32510733
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2017_0404() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/proc/asound/version")) {
            AdbUtils.runPoc("CVE-2017-0404", getDevice(), 60);
        }
        // STS begins the next test before device finishes rebooting,
        // sleep to allow time for device to reboot.
        Thread.sleep(60000);
    }

    /**
     *  b/31225246
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_8412() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/dev/v4l-subdev7")) {
            AdbUtils.runPoc("CVE-2016-8412", getDevice(), 60);
        }
        // STS begins the next test before device finishes rebooting,
        // sleep to allow time for device to reboot.
        Thread.sleep(60000);
    }

    /*
     *  b/28799389
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_3865() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/dev/rmi0")) {
            AdbUtils.runPoc("CVE-2016-3865", getDevice(), 60);
        }
    }

    /**
     *  b/31253168
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2015_7872() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runPoc("CVE-2015-7872", getDevice(), 60);
    }

    /**
     *  b/31095224
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_6787() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runPoc("CVE-2016-6787", getDevice(), 60);
    }

  /**
  * b/31494725
  */
  @RootPermissionTest
  @Test
  public void testPocCVE_2016_8401() throws Exception {
    enableAdbRoot(getDevice());
    if(containsDriver(getDevice(), "/sys/kernel/debug/ion/clients/qseecom-kernel-0")){
      String out = AdbUtils.runPoc("CVE-2016-8401", getDevice());
      assertNotMatches("[\\s\\n\\S]*qsecom:[\\s\\n]*[0-9a-fA-F]+ :[\\s\\n]*[0-9a-fA-F]+" +
                       " : [a-f0-9A-F]{7,15}[^0][\\s\\n\\S]*", out);
    }
  }

    /**
     *  b/31496950
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_8404() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/sys/kernel/debug/ghsic_ctrl_xport/status")) {
            String out = AdbUtils.runPoc("CVE-2016-8404", getDevice(), 60);
            assertNotMatches("[\\s\\n\\S]*INFO_LEAK=#PORT:*[0-9]+[\\s\\n]*port:" +
                             "[\\s\\n]*[0-9a-fA-F]+[\\s\\n\\S]*", out);
        }
    }

    /**
     *  b/31495348
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_8403() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/sys/kernel/debug/ghsic_data_xport/status")) {
            String out = AdbUtils.runPoc("CVE-2016-8403", getDevice(), 60);
            assertNotMatches("[\\s\\n\\S]*INFO_LEAK=#PORT:*[0-9]+[\\s\\n]*port#:" +
                             "[\\s\\n]*[0-9a-fA-F]+[\\s\\n\\S]*", out);
        }
    }

    /*
     *  b/31910462
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_6777() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/dev/nvhost-gpu")) {
            AdbUtils.runPoc("CVE-2016-6777", getDevice(), 60);
        }
    }

    /*
     *  b/29464815
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_6756() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runPoc("CVE-2016-6756", getDevice(), 60);
    }

   /**
     *  b/31252384
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_6791() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runCommandLine("dmesg -c" ,getDevice());
        AdbUtils.runCommandLine("setenforce 0",getDevice());
        if(containsDriver(getDevice(), "/dev/msm_multi_aac") &&
           containsDriver(getDevice(), "/dev/ion")) {
            AdbUtils.runPoc("CVE-2016-6791", getDevice(), 60);
            String dmesgOut = AdbUtils.runCommandLine("cat /sys/fs/pstore/console-ramoops",
                              getDevice());
            assertNotMatches("[\\s\\n\\S]*CVE-2016-6791 Tainted" + "[\\s\\n\\S]*" +
                             "Fatal exception", dmesgOut);
        }
    }

    /*
     *  b/30740545
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_6755() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/dev/v4l-subdev8")) {
            AdbUtils.runPoc("CVE-2016-6755", getDevice(), 60);
        }
    }

    /**
     *  b/31746399
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2014_9910() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runPoc("CVE-2014-9910", getDevice(), 60);
    }

    /**
     *  b/31676542
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2014_9909() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runPoc("CVE-2014-9909", getDevice(), 60);
    }

    /**
     *  b/31495687
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_8409() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/sys/kernel/debug/nvmap/iovmm/maps")) {
            assertFalse("VULNERABLE EXIT CODE FOUND",
                    AdbUtils.runPocCheckExitCode("CVE-2016-8409", getDevice(), 60));
        }
    }
}
