/**
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.security.sts;

import android.platform.test.annotations.RootPermissionTest;
import com.android.tradefed.testtype.DeviceJUnit4ClassRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(DeviceJUnit4ClassRunner.class)
public class Poc18_03 extends SecurityTestCase {

    /**
     * b/70526702
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2017_13252() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runCommandLine("logcat -c", getDevice());
        AdbUtils.runPoc("CVE-2017-13252", getDevice(), 30);
        String output = AdbUtils.runCommandLine("logcat -d", getDevice());
        assertNotMatchesMultiLine(".*>>> /system/bin/mediadrmserver <<<[^\\n]*\\n" +
                                  "[^\\n]*signal 11 \\(SIGSEGV\\).*", output);
        // CTS begins the next test before device finishes rebooting,
        // Wait for device to reboot
        getDevice().waitForDeviceAvailable(60000);
    }
}
