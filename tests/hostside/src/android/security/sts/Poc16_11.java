/**
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.security.sts;

import android.platform.test.annotations.RootPermissionTest;
import com.android.tradefed.testtype.DeviceJUnit4ClassRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(DeviceJUnit4ClassRunner.class)
public class Poc16_11 extends SecurityTestCase {

   /**
     *  b/30149174
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_6753() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(),"/sys/fs/cgroup")) {
            String mount_cmd =
            "mount -t cgroup -o debug debug /sys/fs/cgroup";
            AdbUtils.runCommandLine(mount_cmd, getDevice());
            String run_cmd =
            "cat /sys/fs/cgroup/debug.cgroup_css_links";
            String result = AdbUtils.runCommandLine(run_cmd, getDevice());
            assertNotMatches("[\\s\\n\\S]*[\\s](?!0{16})[a-fA-F0-9]{16}[\\s][\\s\\n\\S]*",result);
        }
    }

   /**
     *  b/32636619
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_6745() throws Exception {
        if(containsDriver(getDevice(),
           "/sys/devices/soc.0/f9924000.i2c/i2c-2/2-0070/input/input0/imagesize")) {
            enableAdbRoot(getDevice());
            AdbUtils.runPoc("CVE-2016-6745", getDevice(), 60);
        }
    }

   /**
     *  b/30799828
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_6742() throws Exception {
        if(containsDriver(getDevice(),
           "/sys/devices/soc.0/f9924000.i2c/i2c-2/2-0020/imagesize")) {
            enableAdbRoot(getDevice());
            AdbUtils.runPoc("CVE-2016-6742", getDevice(), 60);
        }
    }

    /*
     *  b/30937462
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_6743() throws Exception {
        if(containsDriver(getDevice(),
           "/sys/devices/soc.0/f9924000.i2c/i2c-2/2-0020/imagesize") &&
           containsDriver(getDevice(),
           "/sys/devices/soc.0/f9924000.i2c/i2c-2/2-0020/data") ) {
            enableAdbRoot(getDevice());
            AdbUtils.runPoc("CVE-2016-6743", getDevice(), 60);
        }
    }

    /**
     *  b/30311977
     */
    @RootPermissionTest(minPatchLevel = "2016-11")
    @Test
    public void testPocCVE_2016_3904() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runPoc("CVE-2016-3904", getDevice(), 60);
    }

    /**
     *  b/30312054
     */
    @RootPermissionTest(minPatchLevel = "2016-11")
    @Test
    public void testPocCVE_2016_6750() throws Exception {
        enableAdbRoot(getDevice());
        String result = AdbUtils.runCommandLine("cat /sys/kernel/debug/smp2p/int_stats" , getDevice());
        assertMatches("[\\s\\n\\S]*ffffff8001472008[\\s\\n\\S]*", result);
    }
}
