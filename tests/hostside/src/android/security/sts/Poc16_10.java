/**
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.security.sts;

import android.platform.test.annotations.RootPermissionTest;
import com.android.tradefed.testtype.DeviceJUnit4ClassRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(DeviceJUnit4ClassRunner.class)
public class Poc16_10 extends SecurityTestCase {
    /**
     *  b/32919560
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_3935() throws Exception {
      enableAdbRoot(getDevice());
      if(containsDriver(getDevice(), "/dev/qce")) {
        AdbUtils.runPoc("CVE-2016-3935", getDevice(), 120);
      }
    }
   /**
     *  b/30142668
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2015_8951() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), " /dev/snd/pcmC0D16c")) {
            AdbUtils.runPoc("CVE-2015-8951", getDevice(), 60);
        }
    }

    //High
    /**
     *  b/30874196
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_3939() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/dev/graphics/fb0")) {
            AdbUtils.runPoc("CVE-2016-3939", getDevice(), 60);
        }
    }

    /**
     *  b/30204201
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_6673() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/dev/camera.pcl")) {
            AdbUtils.runPoc("CVE-2016-6673", getDevice(), 300);
        }
    }

    /**
     *  b/29953313
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_3902() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/dev/wwan_ioctl")) {
            AdbUtils.runPoc("CVE-2016-3902", getDevice(), 60);
        }
    }

    /**
     *  b/30152501
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_6682() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/dev/msm_aac_in")) {
            enableAdbRoot(getDevice());
            AdbUtils.runCommandLine("logcat -c" , getDevice());
            AdbUtils.runPoc("CVE-2016-6682", getDevice(), 60);
            String logcat =  AdbUtils.runCommandLine("logcat -d", getDevice());
            assertMatches("[\\s\\n\\S]*CVE-2016-6682 stack info: 0x0[\\s\\n\\S]*", logcat);
        }
    }

    /*
     *  b/30148546
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_3910() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runCommandLine("logcat -c" , getDevice());
        AdbUtils.runPoc("CVE-2016-3910", getDevice(), 60);
        String logcat =  AdbUtils.runCommandLine("logcat -d", getDevice());
        assertNotMatches("[\\s\\n\\S]*Fatal signal 11 \\(SIGSEGV\\)" +
                "[\\s\\n\\S]*>>> /system/bin/" +
                "mediaserver <<<[\\s\\n\\S]*", logcat);
    }

    /**
     *  b/29982678
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_6680() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runCommandLine("dmesg -c" , getDevice());
        AdbUtils.runPoc("CVE-2016-6680", getDevice(), 60);
        String dmesgOut = AdbUtils.runCommandLine("dmesg", getDevice());
        assertNotMatches("[\\s\\n\\S]*Calling CRDA for country[\\s\\n\\S]*", dmesgOut);
    }

    /**
     *  b/29999161
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_3901() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/dev/qce")) {
            AdbUtils.runPoc("CVE-2016-3901", getDevice(), 60);
        }
    }

    /**
     *  b/30148243
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_6684() throws Exception {
        enableAdbRoot(getDevice());
        if(containsDriver(getDevice(), "/sys/kernel/debug/sync")) {
            String result = AdbUtils.runCommandLine("cat /sys/kernel/debug/sync", getDevice());
            assertNotMatches("[\\s\\n\\S]*\\[(?!0{16})[a-fA-F0-9]{16}\\][\\s\\n\\S]*",result);
        }
    }

    /**
     *  b/30102557
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_3934() throws Exception {
        enableAdbRoot(getDevice());
        if (containsDriver(getDevice(), "/dev/v4l-subdev12")) {
            AdbUtils.runPoc("CVE-2016-3934", getDevice(), 60);
        }
    }

    /**
     *  b/30152182
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_6681() throws Exception {
        enableAdbRoot(getDevice());
        if (containsDriver(getDevice(), "/dev/msm_wmapro")) {
            AdbUtils.runCommandLine("logcat -c", getDevice());
            AdbUtils.runPoc("CVE-2016-6681", getDevice(), 60);
            String logcat = AdbUtils.runCommandLine("logcat -d", getDevice());
            assertMatches("[\\s\\n\\S]*CVE-2016-6681 No Kernel Information Leak" +
                    "[\\s\\n\\S]*", logcat);
        }
    }

    /**
     *  b/29508816
     */
    @RootPermissionTest(minPatchLevel = "2016-10")
    @Test
    public void testPocCVE_2015_8955() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runCommandLine("dmesg -c" ,getDevice());
        AdbUtils.runPoc("CVE-2015-8955", getDevice(), 60);
        String dmesgOut = AdbUtils.runCommandLine("dmesg", getDevice());
        assertNotMatches("[\\s\\n\\S]*CVE-2015-8955 Tainted" + "[\\s\\n\\S]*", dmesgOut);
    }
}
