/**
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.security.sts;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import android.platform.test.annotations.RootPermissionTest;
import com.android.tradefed.testtype.DeviceJUnit4ClassRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(DeviceJUnit4ClassRunner.class)
public class Poc17_01 extends SecurityTestCase {

   /**
     *  b/32636619
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2017_0429() throws Exception {
        if(containsDriver(getDevice(), "/dev/nvhost-as-gpu")) {
            enableAdbRoot(getDevice());
            AdbUtils.runPoc("CVE-2017-0429", getDevice(), 60);
        }
    }

   /**
     *  b/32219121
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_8455() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runPoc("CVE-2016-8455", getDevice(), 60);
    }

   /**
     *  b/32219255
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_8456() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runPoc("CVE-2016-8456", getDevice(), 60);
        // CTS begins the next test before device finishes rebooting,
        // sleep to allow time for device to reboot.
        Thread.sleep(60000);
    }

   /**
     *  b/32219453
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_8457() throws Exception {
        enableAdbRoot(getDevice());
        AdbUtils.runPoc("CVE-2016-8457", getDevice(), 60);
        // Device takes up to 60 seconds to crash after PoC run.
        Thread.sleep(60000);
    }

    /**
     *  b/32591129
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_8475() throws Exception {
        if(containsDriver(getDevice(), "/dev/laser_stmvl53l0")) {
            enableAdbRoot(getDevice());

            AdbUtils.runCommandLine("logcat -c" , getDevice());
            AdbUtils.runPoc("CVE-2016-8475", getDevice(), 60);

            String logcat =  AdbUtils.runCommandLine("logcat -d", getDevice());
            assertMatches("[\\s\\n\\S]*kernel stack information isn't leaked[\\s\\n\\S]*", logcat);
	}
    }

   /**
     *  b/31799972
     */
    @RootPermissionTest
    @Test
    public void testPocCVE_2016_8474() throws Exception {
        if(containsDriver(getDevice(), "/dev/stmvl6180_ranging")) {
            enableAdbRoot(getDevice());
            AdbUtils.runCommandLine("logcat -c" , getDevice());
            AdbUtils.runPoc("CVE-2016-8474", getDevice(), 60);

            String logcat =  AdbUtils.runCommandLine("logcat -d", getDevice());
            assertNotMatches("[\\s\\n\\S]*VL6180_IOCTL_SETOFFSET, information leak[\\s\\n\\S]*",
                             logcat);
        }
    }

    /**
     *  b/31795790
     */
    @RootPermissionTest(minPatchLevel = "2017-01")
    @Test
    public void testPocCVE_2016_8473() throws Exception {
        if(containsDriver(getDevice(), "/dev/stmvl6180_ranging")) {
            enableAdbRoot(getDevice());
            AdbUtils.runPoc("CVE-2016-8473", getDevice(), 60);

            // Wait for the result
            Thread.sleep(1);
            String result = AdbUtils.runCommandLine("cat /persist/calibration/xtalk", getDevice());
            if(result.trim().equals("65")) {
                assertTrue(true);
            } else {
                assertTrue(false);
            }
        }
    }
}
