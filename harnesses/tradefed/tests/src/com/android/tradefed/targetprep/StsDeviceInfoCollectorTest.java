/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.tradefed.targetprep;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/** Unit tests for {@link StsDeviceInfoCollector}. */
@RunWith(JUnit4.class)
public class StsDeviceInfoCollectorTest {

  @Test
  public void testEmptyFingerprint() throws Exception {
    String overrideFingerprint;
    try {
      overrideFingerprint = StsDeviceInfoCollector.overrideBuildFingerprint("");
    } catch (TargetSetupError e) {
      return;
    }
    Assert.fail(
        "Got fingerprint " + overrideFingerprint + " when there should have been an exception!");
  }

  @Test
  public void testInvalidFingerprint() throws Exception {
    String overrideFingerprint;
    try {
      overrideFingerprint = StsDeviceInfoCollector.overrideBuildFingerprint("a malformed string");
    } catch (TargetSetupError e) {
      return;
    }
    Assert.fail(
        "Got fingerprint " + overrideFingerprint + " when there should have been an exception!");
  }


  @Test
  public void testValidFingerprint() throws Exception {
    String overrideFingerprint =
        StsDeviceInfoCollector.overrideBuildFingerprint(
            "brand/product/device:release/id/incremental:type/tags");
    Assert.assertEquals(
        "brand/product/device:release/id/incremental:user/release-keys", overrideFingerprint);
  }
}
